public class Account extends DoTransaction {

    private Long id;
    private Long userid;
    private String account;
    private String system;
    private int balance;
    private int startBalance;

    public Account() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserid() {
        return userid;
    }

    public void setUserid(Long userid) {
        this.userid = userid;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getSystem() {
        return system;
    }

    public void setSystem(String system) {
        this.system = system;
    }

    public int getBalance() {
        return balance;
    }

    public void setBalance(int balance) {
        this.balance = balance;
    }

    public int getStartBalance() {
        return startBalance;
    }

    public void setStartBalance(int startBalance) {
        this.startBalance = startBalance;
    }

    @Override
    void doTransaction(Account accountFrom, Account accountTo, String system, int amout) {
        int debitAmount = accountFrom.balance - amout;
        System.out.println("На вашем счету :" + debitAmount);

        int operationCredit = accountTo.balance + amout;
        System.out.println("Счет" + accountTo + "Успешно пополнено");

    }

    @Override
    public String toString() {
        return "Account{" +
                "id=" + id +
                ", userid=" + userid +
                ", account='" + account + '\'' +
                ", system='" + system + '\'' +
                ", balance=" + balance +
                ", startBalance=" + startBalance +
                '}';
    }
}
