public class Genre {
    protected String attribute;
    public Genre(String attribute) {
        this.attribute = attribute;
    }

    public String getAttribute() {
        return attribute;
    }

    public void setAttribute(String attribute) {
        this.attribute = attribute;
    }

    @Override
    public String toString() {
        return "Genre{" +
                "attribute='" + attribute + '\'' +
                '}';
    }

    public String getAttributeOfGenre() {
        return "No genre";
    }

    public String getGenreName() {
        return "No genre";
    }
}
