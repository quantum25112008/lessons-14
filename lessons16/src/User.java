public class User implements Administrator, Librarian, Supplier, Reader{
    private String firstName;
    private String lastName;
    private String phone;
    private String email;
    private String address;
    private String role;

    public User() {
    }

    public User(String firstName, String lastName, String phone, String email, String address) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.phone = phone;
        this.email = email;
        this.address = address;
    }

    @Override
    public void findBook(String book) {
        System.out.println("User ищет книгу :" + book);
    }

    @Override
    public void issueBook(String book, Reader reader) {
        System.out.println("Администратор выдал книгу :" + book + "читателю" + reader);
    }

    @Override
    public void overdueNotification(Reader reader) {

    }

    @Override
    public void orderBook(String book) {
        System.out.println("User заказал книгу :" + book);
    }

    @Override
    public void takeBook(String book) {
        System.out.println("User взял книгу :" + book);
    }

    @Override
    public void returnBook(String book) {
        System.out.println("User принес книгу :" + book);
    }

    @Override
    public void deliverBook(String book) {
        System.out.println("Supplier привез книгу :" + book);
    }

    @Override
    public String toString() {
        return "User{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", phone='" + phone + '\'' +
                ", email='" + email + '\'' +
                ", address='" + address + '\'' +
                ", role='" + role + '\'' +
                '}';
    }
}
