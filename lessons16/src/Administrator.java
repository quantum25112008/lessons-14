public interface Administrator {
void findBook(String book);
void issueBook(String book, Reader reader);
   void overdueNotification(Reader reader);
}
