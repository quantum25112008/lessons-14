public interface Reader {
    void takeBook(String book);
    void returnBook(String book);
};
